//import Vue from 'vue';
import Vue = require('vue');
import createPlugin from '../src/index';
import { IMobxVueOptions, IComponentStoreOptions, MobxComponent } from "../src/types";
import store, { resetStore, Store } from './store';
import createComponent from './create-component';

Vue.use<IMobxVueOptions>(createPlugin(store, {enableDeepSubscribe: true}));

afterEach(() => {
  resetStore();
});

describe('tests', () => {

  let testArgs: any[] = [
    ['selectors array', createComponent(false, 'array', 'array')],
    ['selectors map  ', createComponent(false, 'object', 'object')],
    ['decorators     ', createComponent(true)]
  ];

  function runTests(type: string, vm: Vue) {   
   
    test(`${type}: maps state selectors`, () => {  
      
      const countEl = vm.$el.querySelector('#count') as Element;  
      const count2El = vm.$el.querySelector('#count2') as Element;
      const favGenreEl = vm.$el.querySelector('#favGenre') as Element;
      const favMovieEl = vm.$el.querySelector('#favMovie') as Element;
      const secondMovieEl = vm.$el.querySelector('#movies li:nth-child(2)') as Element;

      expect(countEl.textContent).toBe('2');  
      expect(count2El.textContent).toBe('2');
      expect(favGenreEl.textContent).toBe('Fantasy');
      expect(favMovieEl.textContent).toBe('Alien 1979');
      expect(secondMovieEl.textContent).toBe('Gravity');
    });

    test(`${type}: re-renders component on state change`, (done: jest.DoneCallback) => {

      store.addMovie('Gifted', 2017);

      vm.$nextTick(function() {
        const countEl = vm.$el.querySelector('#count') as Element;
        expect(countEl.textContent).toBe('3'); 
        done();
      });
    });

    test(`${type}: can invoke actions on instance store object`, (done: jest.DoneCallback) => {
      
      (<MobxComponent<Store>>vm.$children[0]).$store.addMovie('Gifted', 2017);

      vm.$nextTick(function() {
        const countEl = vm.$el.querySelector('#count') as Element;
        expect(countEl.textContent).toBe('3'); 
        done();
      });
    });

    test(`${type}: maps action methods`, () => {
      
      expect((<MobxComponent<Store>>vm.$children[0]).add).toBeDefined();
      expect((<MobxComponent<Store>>vm.$children[0]).update).toBeDefined();
    });

    test(`${type}: invoking action in component re-renders component`, (done: jest.DoneCallback) => {
      
      (<MobxComponent<Store>>vm.$children[0]).add('Logan', 2017);

      vm.$nextTick(function() {
        const countEl = vm.$el.querySelector('#count') as Element;
        const movieEl = vm.$el.querySelector('#movies li:nth-child(3)') as Element;
        expect(countEl.textContent).toBe('3'); 
        expect(movieEl.textContent).toBe('Logan');
        done();
      });
    });

    test(`${type}: updating object in observable array re-renders component`, (done: jest.DoneCallback) => {
      
      (<MobxComponent<Store>>vm.$children[0]).update('H1Mv8FIMb', 'Aliens');

      vm.$nextTick(function() {    
        const movieEl = vm.$el.querySelector('#movies li:nth-child(1)') as Element;    
        expect(movieEl.textContent).toBe('Aliens');
        done();
      });
    });
  }

  testArgs.forEach(args => {   
    runTests.apply(null, args);
  })

});