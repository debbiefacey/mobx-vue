import Vue = require('vue');
import { CreateElement, VNode } from 'vue';
import Component from 'vue-class-component';
import { IComponentStoreOptions, StateMappings, ActionMappings, MobxComponent } from '../src/types';
import { Action, State } from '../src/decorators';
import { Store, Movie } from "./store";

export type MappingType = 'array' | 'object';

export default function(
  useDecorators: boolean,
  stateMappingType: MappingType = 'object', 
  actionMappingType: MappingType = 'object') {

  @Component
  class ClassComponent extends Vue {

    @State('count')
    count: number;
    
    @State((s:Store) => s.movies)
    movies: Movie[];
    
    @State((s:Store) => s.favs.genre) 
    genre: string;  

    @State('favs.movie')
    favMovie: Movie;

    @State('favs.movie.year')
    favMovieYear: number;

    @Action('addMovie')
    add: any;

    @Action((s:Store) => s.updateMovie)
    update: any;

    render(this: MobxComponent<Store> & ClassComponent, h: CreateElement): VNode {
      return h('div', [
        h('div', {attrs: {id: 'count'}}, this.count.toString()),
        h('div', {attrs: {id: 'count2'}}, this.$store.count.toString()),       
        h('div', {attrs: {id: 'favGenre'}}, this.genre),
        h('div', {attrs: {id: 'favMovie'}}, this.favMovie.title + ' ' + this.favMovieYear),
        h('ul', {attrs: {id: 'movies'}}, function(v: MobxComponent<Store> & ClassComponent){
          return v.movies.map(m => h('li', m.title))
        }(this))
      ])
    }
  }
  
  let mobxState: StateMappings<Store>;

  switch(stateMappingType) {
    case 'array':
      mobxState = ['count', 'movies', 'favs.genre', 'favs.movie:favMovie', 'favs.movie.year:favMovieYear']; break;
    case 'object':
      mobxState = { 
        count : 'count', 
        genre: 'favs.genre', 
        movies: s => s.movies,
        favMovie: s => s.favs.movie,
        favMovieYear: s => s.favs.movie.year,
      }; break
    default: throw new Error('Invalid mapping type');
  }

  let mobxActions: ActionMappings<Store>;

  switch(actionMappingType) {
    case 'array':
      mobxActions = ['addMovie:add', 'updateMovie:update']; break;
    case 'object':
      mobxActions = { 
        'add': 'addMovie',
        'update': s => s.updateMovie
      }; break;
    default: throw new Error('Invalid mapping type');
  }

  const OptionsObject: IComponentStoreOptions<Store> = {   
    mobxState,
    mobxActions,
    render(h: CreateElement) {
      return h('div', [
        h('div', {attrs: {id: 'count'}}, this.count),
        h('div', {attrs: {id: 'count2'}}, this.$store.count.toString()),       
        h('div', {attrs: {id: 'favGenre'}}, this.genre),
        h('div', {attrs: {id: 'favMovie'}}, this.favMovie.title + ' ' + this.favMovieYear),
        h('ul', {attrs: {id: 'movies'}}, function(v: MobxComponent<Store>){
          return (<Movie[]>v.movies).map(m => h('li', m.title))
        }(this))
      ])
    } 
  };
  
  const Child = useDecorators ? ClassComponent : OptionsObject ;
  
  const vm = new Vue({
    el: document.createElement('div'),
    components: {
      Child
    },
    render: (h: CreateElement) => h(Child) 
  });

  return vm;
}