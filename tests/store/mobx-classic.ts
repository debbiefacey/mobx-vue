import { observable, action, computed, autorun } from 'mobx';
//import shortid from 'shortid';
import shortid = require('shortid');

export class Movie {
  constructor(title: string, year: number, id?: string) {
    this.id = id || shortid.generate();
    this.title = title;
    this.year = year;
  }

  @observable id: string;
  @observable title: string;
  @observable year: number;
}

export class Favorites {
  
  constructor(actor: string, genre: string, movieId: string) {
    this.actor = actor;
    this.genre = genre;
    this.movieId = movieId;
  } 

  @observable actor: string;
  @observable genre: string;
  @observable movieId: string;
}

export class Store {
  @observable _favs: Favorites; 
  @observable movies: Movie[];
   
  
  constructor(favs?: Favorites, movies?: Movie[]) {
    if(favs) {
      this._favs = favs;      
    }
    if(movies) {
      this.movies = movies
    }
  }

  @computed get count() {
    return this.movies.length;
  }

  @computed get favs() {
    return {
      actor: this._favs.actor,
      genre: this._favs.genre,
      movie: this.movies.filter(m => m.id === this._favs.movieId)[0]
    };
  }

  @action addMovie(title: string, year: number) {
    this.movies.push(new Movie(title, year));
  }

  @action updateMovie(id: string, title?: string, year?: number) {    
   let movies = this.movies.filter(item => item.id === id);   
    if (movies.length > 0) {      
      if(title) movies[0].title = title;
      if(year) movies[0].year = year;
    }
  }

  @action __reset(favs: Favorites, movies: Movie[]) {    
    this._favs.genre = favs.genre;
    this._favs.actor = favs.actor;
    this._favs.movieId = favs.movieId

    while (this.movies.length > movies.length) {
       this.movies.pop();
     }
     this.movies.forEach((m, i) => {
       m.title = movies[i].title;
       m.year = movies[i].year;
     })
  }
};

function createSnapShot() {
  return {
    favs: new Favorites('Sigourney Weaver', 'Fantasy', 'H1Mv8FIMb'),
    movies: [
      new Movie('Alien', 1979, 'H1Mv8FIMb'),
      new Movie('Gravity', 2013, 'B10OUYLzW')
    ]
  };
}
const {favs, movies} = createSnapShot();
const store = new Store(favs, movies);

export function resetStore() {
  const {favs, movies} = createSnapShot();
  store.__reset(favs, movies);
}

export default store;

