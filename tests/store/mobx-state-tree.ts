import { types, applySnapshot } from 'mobx-state-tree';
import shortid = require('shortid');

const MovieModel = types.model('Movie', {
  id: types.identifier(),
  title: types.string,
  year: types.number
});

const Favorites = types.model('Favorites', {
  actor: types.string,
  genre: types.string,
  movie: types.reference(MovieModel)
});

const StoreModel = types.model('Store', {
  favs: Favorites,
  movies: types.array(MovieModel),
  get count() {
    return this.movies.length;
  }
},{
  addMovie(title: string, year: number) {
    this.movies.push({id: shortid.generate(), title, year});
  },

  updateMovie(id: string, title?: string, year?: number) {    
    let movie = this.movies.find(m => m.id === id);
    if (movie) {
      if(title) movie.title = title;
      if(year) movie.year = year;
    }
  }
})

const snapShot = {
  favs: {
    actor: 'Sigourney Weaver', 
    genre: 'Fantasy', 
    movie: 'H1Mv8FIMb'
  },
  movies : [
    {
      id: 'H1Mv8FIMb',
      title: 'Alien',
      year: 1979
    },
    {
      id: 'B10OUYLzW',
      title: 'Gravity',
      year: 2013
    }
  ]
};

type Movie = typeof MovieModel.Type;
type Store = typeof StoreModel.Type;

const store: Store = StoreModel.create(snapShot);

function resetStore() {
  applySnapshot(store, snapShot);
}

export { resetStore, Store, Movie};
export default store;


