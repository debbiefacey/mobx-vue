const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: './build/src/index.js',
  output: {
    libraryTarget: 'umd',
    library: 'mobxVue',
    path: path.resolve(__dirname, 'dist'),
    filename: 'mobx-vue.js'
  },
  resolve: {
    extensions: ['.js']
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015'],
          plugins: ['transform-class-properties']
        }
      }
    ]
  },
  externals: {
    'vue': {
      commonjs: 'vue',
      commonjs2: 'vue',
      amd: 'vue',
      root: 'Vue'
    },
    'mobx': 'mobx'
  }
};