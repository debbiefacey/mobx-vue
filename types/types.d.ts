import { IReactionDisposer } from 'mobx';
import Vue, { ComponentOptions } from 'vue';
export interface IMobxVueOptions {
    storePropName?: string;
    enableDeepSubscribe?: boolean;
}
export interface IStateSelectorFunction<TStore> {
    (store: TStore): any;
}
export interface IActionSelectorFunction<TStore> {
    (store: TStore): Function;
}
export interface MappingObject<TStore> {
    [key: string]: string | ((store: TStore) => any);
}
export interface StateMappingObject<TStore> extends MappingObject<TStore> {
    [key: string]: string | IStateSelectorFunction<TStore>;
}
export interface ActionMappingObject<TStore> extends MappingObject<TStore> {
    [key: string]: string | IActionSelectorFunction<TStore>;
}
export declare type StateMappings<TStore> = string[] | StateMappingObject<TStore>;
export declare type ActionMappings<TStore> = string[] | ActionMappingObject<TStore>;
export interface IComponentStoreOptions<TStore> extends ComponentOptions<MobxComponent<TStore>> {
    mobxState: StateMappings<TStore>;
    mobxActions?: ActionMappings<TStore>;
}
export interface MobxComponent<TStore> extends Vue {
    new (options?: IComponentStoreOptions<TStore>): MobxComponent<TStore>;
    $options: IComponentStoreOptions<TStore>;
    $store: TStore;
    __mobxReactionDisposers: IReactionDisposer[];
    [key: string]: any;
}
