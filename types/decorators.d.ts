import { IStateSelectorFunction, IActionSelectorFunction } from './types';
export declare function State<TStore>(selector: string | IStateSelectorFunction<TStore>): PropertyDecorator;
export declare function Action<TStore>(selector: string | IActionSelectorFunction<TStore>): PropertyDecorator;
