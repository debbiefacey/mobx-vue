import { PluginObject } from 'vue';
import { IMobxVueOptions } from './types';
declare function createPlugin<TStore>(store: TStore, options?: IMobxVueOptions): PluginObject<IMobxVueOptions>;
export default createPlugin;
