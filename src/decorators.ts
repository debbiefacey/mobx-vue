import { createDecorator } from 'vue-class-component';
import { IStateSelectorFunction, IComponentStoreOptions, StateMappingObject, IActionSelectorFunction, ActionMappingObject } from './types';

export function State<TStore>(selector: string | IStateSelectorFunction<TStore>): PropertyDecorator {
  return createDecorator((componentOptions: IComponentStoreOptions<TStore>, key) => {
          
    if (!componentOptions.mobxState) {      
      componentOptions.mobxState = {};
    }    
    (<StateMappingObject<TStore>>componentOptions.mobxState)[key] = selector;

  })
}

export function Action<TStore>(selector: string | IActionSelectorFunction<TStore>): PropertyDecorator {
  return createDecorator((componentOptions: IComponentStoreOptions<TStore>, key) => {
    
    if (!componentOptions.mobxActions) {
      componentOptions.mobxActions = {};
    }    
    (<ActionMappingObject<TStore>>componentOptions.mobxActions)[key] = selector;
  })
}