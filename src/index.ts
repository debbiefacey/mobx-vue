import { reaction, IReactionDisposer, Reaction, isObservable, toJS } from 'mobx';
import Vue, { ComponentOptions, PluginObject } from 'vue';
import { IMobxVueOptions, MobxComponent, StateMappings, ActionMappings, IStateSelectorFunction, StateMappingObject, ActionMappingObject, MappingObject } from './types';

function createPlugin<TStore>(store: TStore, options: IMobxVueOptions = {}): PluginObject<IMobxVueOptions> {

  const plugin: PluginObject<IMobxVueOptions> = {
    install: function (VueCtor: typeof Vue) { 
      
      Object.defineProperty(VueCtor.prototype, options.storePropName || '$store', {
        get: function() {
          return store;
        }
      });
               
      VueCtor.mixin({
        created: function(this: MobxComponent<TStore>) {    
          registerStore(this, options);
        },

        destroyed: function(this: MobxComponent<TStore>) {          
          disposeReactions(this);
        },

        beforeMount: function(this: MobxComponent<TStore>) {
        }
      });
    }
  };

  return plugin;
}

function registerStore<TStore>(
  component: MobxComponent<TStore>,
  storeOptions: IMobxVueOptions) {
  component.__mobxReactionDisposers = [];
        
  const state = component.$options.mobxState;
  const actions = component.$options.mobxActions; 
  
  registerState(component, state, storeOptions.enableDeepSubscribe || false);
  if(actions) {
    registerActions(component, actions);
  }
}

function registerState<TStore>(
  component: MobxComponent<TStore>, state: StateMappings<TStore>,
  shouldDeepSubscribe: boolean) {
  
  if(!state) return;

  const mappingsObj = ensureMappingsObject(state);
  
  for (let key in mappingsObj) {
    let selector = mappingsObj[key];     
    let selectorFn: Function;

    if (typeof selector === 'string') {
      selectorFn = () => getProp(component.$store, <string>selector);
    }
    else if (typeof selector === 'function') {
      selectorFn = () => (<Function>selector)(component.$store);      
    }
    else {
      continue;
    }

    addStatePropertyAndSubscribe(component, key, selectorFn, shouldDeepSubscribe);
  }
}

function registerActions<TStore>(component: MobxComponent<TStore>, actions: ActionMappings<TStore>) {  
  
  if(!actions) return;

  const mappingsObj = ensureMappingsObject(actions);

  for (let key in mappingsObj) {
    let selector = mappingsObj[key];     
    let action: Function;

    if (typeof selector === 'string') {
      action = getProp(component.$store, selector)  
    }
    else if (typeof selector === 'function') {
      action = selector(component.$store);      
    }
    else {
      continue;
    }

    component[key] = action.bind(component.$store)
  }
}

function addStatePropertyAndSubscribe<TStore>(
  component: MobxComponent<TStore>,
  propName: string,
  stateSelectorFn: Function,
  shouldDeepSubscribe: boolean
  ) {

  function createReaction(stateSelectorFn: Function) {
    let disposer = reaction(
      () => stateSelectorFn(),
      () => component.$forceUpdate()
    );  
    component.__mobxReactionDisposers.push(disposer);
  }
 
  function deepSubscribe(state: any) {
    if (!isObservable(state)) return;

    if (Array.isArray(toJS(state))) {
      createReaction(() => state.slice());
      state.forEach((obj: any) => deepSubscribe(obj));
    }
    else {
      Object.keys(state).forEach(key => {
        createReaction(() => state[key]);
        deepSubscribe(state[key]);
      });
    }
  }

  if (!component.hasOwnProperty(propName)) {
    Object.defineProperty(component, propName, {
      get: function() {
        return stateSelectorFn();
      }
    });  
  }    

  createReaction(stateSelectorFn);

  if (shouldDeepSubscribe) deepSubscribe(stateSelectorFn());
}

function ensureMappingsObject<TStore>(
  mappings: StateMappings<TStore> | ActionMappings<TStore>) {    
  let mappingsObj: MappingObject<TStore>;
    
  if(Array.isArray(mappings)) {
    const stringSelectorRegex = /^(\w+(\.\w+)*)(:(\w+))?$/i;

    mappingsObj = mappings.reduce((prev: any, propName: string) => {      
      let matches;
      if((matches = stringSelectorRegex.exec(propName)) !== null) {        
        let selector = matches[1];
        let key = matches[4];
        if(!key) {
          let indexOfDot = selector.lastIndexOf('.');
          key = indexOfDot < 0 ? selector : selector.substr(indexOfDot + 1);
        }
        prev[key] = selector;
      }
      
      return prev;

    }, {});
  }
  else {
    mappingsObj = mappings;
  }

  return mappingsObj;
}

function disposeReactions<TStore>(component: MobxComponent<TStore>) {
  if(component.__mobxReactionDisposers) {
    for(let i in component.__mobxReactionDisposers) {
        component.__mobxReactionDisposers[i]();
    }
  }
}

function getProp(obj: any, path: string) {
	return path.split('.').reduce((o, p) => o[p], obj);
}

export default createPlugin;
